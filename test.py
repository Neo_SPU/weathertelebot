import telebot
import pyowm

owm = pyowm.OWM('6d00d1d4e704068d70191bad2673e0cc', language = "ru")
bot = telebot.TeleBot("868014815:AAHlhouAzgmS1m0HxutGX5jN82Pbmq41EfY")

@bot.message_handler(content_types=['text'])

def function_name(message):
    if len(message.text) < 3:
        weather_bot(message)
    else:
        send_weather(message)

def weather_bot(message):

    welcomeMessage = "Привет, " + message.chat.first_name + "!\n"
    welcomeMessage += "Хочешь узнать погоду?\n"
    welcomeMessage += "Введи название города:\n"

    bot.send_message(message.chat.id, welcomeMessage)

def send_weather(message):
    city = message.text

    if city == ("Харьков" or "Харкив"):
        city = "Kharkiv"

    observation = owm.weather_at_place(city)
    w = observation.get_weather()
    temp = w.get_temperature('celsius')["temp"]

    answer = "В городе " + message.text + " сейчас " + w.get_detailed_status()
    answer += "\nТемпература: " + str(temp) + " С "

    if temp < 10:
        answer += "\nСейчас очень холодно, одевайся очень тепло!"
    elif temp < 20:
        answer += "\nТемпература холодная, одевайся теплее!"
    else:
        answer += "\nТемпература нормальная, одевай что угодно!"
    bot.send_message(message.chat.id, answer)

bot.polling( none_stop = True)
